﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualStudio.DebuggerVisualizers;

namespace DSP
{
	[Serializable]
	class Model
	{
		public readonly List<Wave> Waves = new List<Wave>();


		public double MaxAmp;
		public double MinPeriod;
		public double MaxPeriod;

		public int frameLen;
		public int offsetInFrame;
		public double[] samples;

        //double OffsetY;
        public double[] Polinom;

		public State Bind(double[] p)
		{
            for (int i = 0; i < Polinom.Length; i++)
            {
                Polinom[i] = 2* MaxAmp * (p[i]-0.5);
            }

            var pindex = Polinom.Length;
			foreach (var wave in Waves)
			{
				wave.ReadParams(p, pindex);
                pindex += 3;
			}
            

			double result = 0;
			for (int i = 0; i < frameLen; i++)
			{
				var s = samples[offsetInFrame + i];
                var v = GetValue(i);

				double delta = s - v;
				result += delta * delta;
			}

			return new State {Model = this, Params = p, Value = Math.Sqrt( result )/frameLen};
		}

        public double[] CreateParams()
        {
            var result = new double[this.Length];
            for (int i = 0; i < Polinom.Length; i++)
                result[i] = 0.5;

            return result;
        }

        public double GetValue(int i)
        {
            double ofs = 0;
            for(int p=0; p<Polinom.Length; p++){
                ofs += Polinom[p] * Math.Pow(i/frameLen, p);
            }
            return ofs + Waves.Sum(w => w.GetValue(i));
        }

		public Bitmap DebugView
		{
			get
			{
                
				int h = 600;
				int hh = h/2;
				int hm = h - 1;
				var result = new Bitmap(Math.Max( frameLen, 100), h, PixelFormat.Format16bppRgb555);
					
				double min = 0;
				double max = 0;


				for (int i = 0; i < frameLen; i++)
				{
					var s = samples[offsetInFrame + i];
					if (s > max)
						max = s;

					if (s < min)
						min = s;

					s = GetValue(i);
					if (s > max)
						max = s;

					if (s < min)
						min = s;
				}

				double delta = 2*(max - min);
				double scale = h/delta;


				for (int i = 0; i < frameLen; i++)
				{
					var v = scale * samples[offsetInFrame + i] + hh;
                    if (v > hm) v = hm;
                    if (v < 0) v = 0;
					result.SetPixel(i, (int) v, Color.Red);

					v = scale*GetValue(i) + hh;
                    if (v > hm) v = hm;
                    if (v < 0) v = 0;
					result.SetPixel(i, (int)v, Color.Blue);

					v = scale* (+Waves[0].GetValue(i)) + hh;
					if (v > hm) v = hm;
					if (v < 0) v = 0;
					result.SetPixel(i, (int)v, Color.Green);

                    if (Waves.Count > 1)
                    {
                        v = scale * ( +Waves[1].GetValue(i) )+ hh;
                        if (v > hm) v = hm;
                        if (v < 0) v = 0;
                        result.SetPixel(i, (int)v, Color.Goldenrod);

                    }

                    if (Waves.Count > 2)
                    {
                        v = scale *( + Waves[2].GetValue(i)) + hh;
                        if (v > hm) v = hm;
                        if (v < 0) v = 0;
                        result.SetPixel(i, (int)v, Color.Gray);

                    }



				}

				return result;
			}
		}

		public int Length
		{
			get { return Polinom.Length + Waves.Count*3; }
				
		}
	}


	public class DebuggerSide : DialogDebuggerVisualizer
	{
		override protected void Show(IDialogVisualizerService windowService,
						   IVisualizerObjectProvider objectProvider)
		{
			var s = (State)objectProvider.GetObject();
			s.Model.Bind(s.Params);
			Image image = s.Model.DebugView;

			Form form = new Form();
			form.Text = string.Format("Width: {0}, Height: {1}",
									 image.Width, image.Height);
			form.ClientSize = new Size(image.Width, image.Height);
			form.FormBorderStyle = FormBorderStyle.FixedToolWindow;

			PictureBox pictureBox = new PictureBox();
			pictureBox.Image = image;
			pictureBox.Parent = form;
			pictureBox.Dock = DockStyle.Fill;

			windowService.ShowDialog(form);
		}
	}

	[DebuggerVisualizer(typeof(DebuggerSide))]
	[Serializable]
	class State
	{
		public Model Model;
		public double[] Params;
		public double Value;
	}

	[Serializable]
	class Wave
	{
		//public int ParamsIndex;
		public Wave Prev;
		public Model Model;

		public double Amp;
		public double Period;
		public double Phase;
		public double Freq;
		public bool IsNull;

		public void ReadParams(double[] p, int ParamsIndex)
		{
			Amp = p[ParamsIndex] * Model.MaxAmp;
			double minPeriod = Prev == null ? Model.MinPeriod : Prev.Period;
			IsNull = minPeriod >= Model.MaxPeriod;
			if (IsNull)
				return;

            Period = minPeriod + (Model.MaxPeriod - minPeriod) * p[ParamsIndex + 1];
			Phase =  p[ParamsIndex + 2];
			Freq = 2 * Math.PI / Period;

		}
		public double GetValue(double t)
		{
			if (IsNull)
				return 0;

            return Amp * Math.Sin(Freq * t + 2 * Math.PI * Phase);
		}
	}

	class EqSolver
	{

		//public Func<double[], double> Fn;
		public Model Model;
		public int? MaxIter;
		public double? FitValue;
		public int Iter;
		public bool Break()
		{
			if (MaxIter <= Iter)
				return true;

			if (FitValue.HasValue
				&& Math.Abs(FitValue.Value) >= Math.Abs(State.Value))
				return true;

			return false;


		}
		//public double[] Range;
		//public double[] LowerBounds;
		// public double[] Vector;


		public State State;

		readonly Random rnd = new Random(1);
		//  private double[] newVector;

		public void MonteCarlo(params int[] plist)
		{
            Iter = 0;

			

			//for (int i = 0; i < Vector.Length; i++)
			//{
			//	Vector[i] = rnd.NextDouble();

			//}

			//double start = Fn(Vector);
            if(State == null)
			    State = Model.Bind(Model.CreateParams());
            var newVector = new double[Model.Length];

			while (!Break())
			{
				Iter++;
                if (plist.Length > 0)
                {
                    foreach(int i in plist)
                        newVector[i] = rnd.NextDouble();
                }
                else
                {
                    for (int i = 0; i < newVector.Length; i++)
                    {
                        newVector[i] = rnd.NextDouble();

                    }

                }

				var newValue = Model.Bind(newVector);
				if (Math.Abs(newValue.Value) < Math.Abs(State.Value))
				{

					State = newValue;
					newVector = new double[Model.Length];

				}


			}

		}

		//   private double[] direction;

		public void DescentRandomDir()
		{
            Iter = 0;
			double cell = 0.01;

			double[] direction = new double[Model.Length];


			double[] newVector = State.Params.ToArray();


			//State = Model.Bind(State.Params);


            do
            {
                // double directionLen = 0;
                for (int i = 0; i < direction.Length; i++)
                {
                    newVector[i] = State.Params[i];

                    if (newVector[i] == 0)
                    {
                        direction[i] = cell * cell * (rnd.NextDouble() - 0.5);
                    }
                    else
                    {
                        direction[i] = (rnd.NextDouble() - 0.5) * cell * newVector[i];
                    }


                    // newVector[i] += direction[i];
                    //  directionLen += direction[i]*direction[i];

                }
                //directionLen = Math.Sqrt(directionLen);

                //double newValue = Fn(newVector);





                for (int k = 0; k < 10; k++)
                {
                    Iter++;
                    for (int i = 0; i < direction.Length; i++)
                    {

                        newVector[i] += direction[i];

                    }

                    var newValue = Model.Bind(newVector);
                    if (Math.Abs(newValue.Value) < Math.Abs(State.Value))
                    {

                        State = newValue;
                        newVector = (double[])newVector.Clone();


                    }
                    else
                    {
                        break;
                    }
                }


            }
            while (!Break());



		}

        public void DescentGradient()
        {
            Iter = 0;
            double cell = 0.01;

            double[] direction = new double[Model.Length];


            double[] newVector = State.Params.ToArray();


            //State = Model.Bind(State.Params);


            do
            {
                // double directionLen = 0;
                for (int i = 0; i < direction.Length; i++)
                {
                    newVector[i] = State.Params[i];

                    if (newVector[i] == 0)
                    {
                        direction[i] = cell * cell * (rnd.NextDouble() - 0.5);
                    }
                    else
                    {
                        direction[i] = (rnd.NextDouble() - 0.5) * cell * newVector[i];
                    }


                    // newVector[i] += direction[i];
                    //  directionLen += direction[i]*direction[i];

                }
                //directionLen = Math.Sqrt(directionLen);

                //double newValue = Fn(newVector);





                for (int k = 0; k < 10; k++)
                {
                    Iter++;
                    for (int i = 0; i < direction.Length; i++)
                    {

                        newVector[i] += direction[i];

                    }

                    var newValue = Model.Bind(newVector);
                    if (Math.Abs(newValue.Value) < Math.Abs(State.Value))
                    {

                        State = newValue;
                        newVector = (double[])newVector.Clone();


                    }
                    else
                    {
                        break;
                    }
                }


            }
            while (!Break());



        }
        public void DescentPerComponent()
        {
            Iter = 0;
            double cell = 0.01;

            double direction = 0;


            var newVector = State.Params.ToArray();


            //State = Model.Bind(State.Params);


            while (!Break())
            {
                // double directionLen = 0;
                Iter++;
                int i = Iter % Model.Length;
                newVector[i] = State.Params[i];
                // for (int i = 0; i < direction.Length; i++)
                {
                    if (newVector[i] == 0)
                    {
                        direction = cell * cell * (rnd.NextDouble() - 0.5);
                    }
                    else
                    {
                        direction = (rnd.NextDouble() - 0.5) * cell * newVector[i];
                    }


                    // newVector[i] += direction[i];
                    //  directionLen += direction[i]*direction[i];

                }
                //directionLen = Math.Sqrt(directionLen);

                //double newValue = Fn(newVector);





                for (int k = 0; k < 10; k++)
                {

                    

                    newVector[i] += direction;



                    var newValue = Model.Bind(newVector);
                    if (Math.Abs(newValue.Value) < Math.Abs(State.Value))
                    {

                        State = newValue;
                        newVector = (double[])newVector.Clone();


                    }
                    else
                    {
                        break;
                    }
                }


            }



        }


	}

	 class InstantFreq
	{
		public static List<Model> Solve(WFile src, int step, int window, double error, int polinom)
		{
			var result = new List<Model>();

            var solver = new EqSolver();
            double maxAmp = src.Samples.Max();

            for (int pos = 0; pos < src.Samples.Length; pos += step) 
            {
                //сначала маленькое окно, простая модель, увеличиваем окно пока модель перестает подходить, добавляем гармонику
                int w = 10;
                var model = new Model { MinPeriod = 4, 
                    MaxPeriod = src.SampleRateHz/100, MaxAmp = maxAmp, 
                    samples = src.Samples, 
                    offsetInFrame = pos, 
                    Polinom = new double[polinom],
                    frameLen = w };
                model.Waves.Add(new Wave{Model = model});
                solver.Model = model;
                solver.Iter = 0;
                solver.MaxIter = 9000;
                solver.MonteCarlo();

                //var img = model.DebugView;
                
                while(true)
                {
                    solver.Iter = 0;
                    solver.MaxIter = 9000;
                    solver.FitValue = error ;
                    solver.DescentPerComponent();
                    solver.DescentRandomDir();
                    if(solver.State.Value < error){
                        w += 10;
                        model.frameLen = w;
                        solver.State = model.Bind(solver.State.Params);

                    }
                    else{

                        model.Waves.Add(new Wave{Model = model, Prev = model.Waves.Last()});
                        Array.Resize(ref solver.State.Params, model.Length);
                        solver.State = model.Bind(solver.State.Params);
                        solver.Iter = 0;
                        solver.MaxIter = 9000;
                        solver.MonteCarlo(0, model.Length-1, model.Length-2, model.Length-3);
                    }

                    if (model.Waves.Count > 3)
                        break;

                    if (w > window)
                        break;


                }

                result.Add(model);

          

            }
			return result;
		}

		public struct Pt
		{
			public double W;
			public double A;
			public double Ofs;
		}
		
	}
}