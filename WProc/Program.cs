﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DSP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Test();
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }

	    public static void Test()
	    {
		    var path = @"..\..\..\Samples\glasn";
		    var src = WFile.ReadWaveFile(path + ".wav");

            double w =  2 * Math.PI/src.SampleRateHz;
            for (int i = 0; i < src.Samples.Length; i++)
            {
	            src.Samples[i] = 15000*Math.Cos(w*100*i)+
	            25000 * Math.Sin(w * 1000 * i);
            }

            InstantFreq.Solve(src, src.SampleRateHz / 10, src.SampleRateHz / 10, 10, 1);

		   // double frame = src.SampleRateHz*0.01;
		   // WFile.AutoCorr(src, (int)frame, (int)frame, path + "\\acorr" );

			
		    //WFile radius;
		    //WFile phase;
		    //src.ToPolar(out radius, out phase);
		    //radius.SaveToFile(path + "_radius.wav");
		    //phase.SaveToFile(path + "_phase.wav");

		    //{
                
		    //	WFile signal = src.InitNew();
		    //	WFile delta = src.InitNew();
		    //	src.ToSinus(signal, delta);
		    //	signal.SaveToFile(path + "_signal.wav");
		    //	delta.SaveToFile(path + "_delta.wav");
		    //}




		    //{

		    //    var lens = new int[] { 1, 2, 4, 10, 20 };
		    //    foreach (var len in lens)
		    //    {
		    //        var f = src.Clone();
		    //        f.DiffInt(len);
		    //        f.SaveToFile(path + "_di_" + len + ".wav");


		    //    }

		    //}

		    //{
		    //    var lens = new int[] { 1, 2, 4, 6, 8, 10 };
		    //    foreach (var len in lens)
		    //    {
		    //        var f = src.Clone();
		    //        for (int i = 0; i < len; i++)
		    //        {
		    //            f.DiffM();
		    //            f.RemoveTopN(10);
		    //        }


		    //        f.SaveToFile(path + "_d_" + len + ".wav");


		    //    }
		    //}

		    //{
		    //    var lens = new int[] { 1, 2, 3,4 };
		    //    foreach (var len in lens)
		    //    {
		    //        var f = src.Clone();
		    //        f.RemoveTopN(100);

		    //        for (int i = 0; i < len; i++)
		    //        {
		    //            //f.BeforeIntegrate(0.05);
		    //            f.Integrate();
		    //            f.ToCenter(0.01);


		    //        }


		    //        f.SaveToFile(path + "_int_" + len + ".wav");


		    //    }
		    //}




		    //for (int i = 0; i < 10; i++)
		    //{
		    //    var imf = src.GetIMF();
		    //    imf.NormToShort();
		    //    imf.SaveToFile(path + "_imf_" + i + ".wav");
		    //}


		    //src.SaveToFile(path + "C.wav");

		    //var diff = src.Diff();
		    //diff.NormToShort();
		    //diff.SaveToFile(path + "D.wav");

		    //diff = diff.Diff();
		    //diff.NormToShort();
		    //diff.SaveToFile(path + "DD.wav");

		    //src.Center();
		    //var integral = src.Integrate();
		    //integral.NormToShort();
		    //integral.SaveToFile(path + "I.wav");

		    //integral.Center();
		    //integral = integral.Integrate();
		    //integral.NormToShort();
		    //integral.SaveToFile(path + "II.wav");

		    //var b = src.DrawInvert(3000, 800);
		    //b.Save(path + ".bmp");

	    }
    }
}
