﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EricOulashin;

using Microsoft.VisualStudio.DebuggerVisualizers;

namespace DSP
{
    public class WFile
    {
        public double[] Samples;
        public int SampleRateHz;
        public static WFile ReadWaveFile(string path)
        {
            var result = new WFile();
            var src = new WAVFile();
            src.Open(path, WAVFile.WAVFileMode.READ);
            int factor = src.IsStereo ? 2 : 1;
			result.Samples = new double[src.NumSamples / factor];
            result.SampleRateHz = src.SampleRateHz;
            
            for (int i = 0; i < result.Samples.Length; i++ )
            {
               src.SeekToAudioSample(i*factor);
               result.Samples[i] = src.GetNextSampleAs16Bit();
            }
                
            return result;
        }

        public void SaveToFile(string path)
        {
            var dest = new WAVFile();
            dest.Create(path, false, SampleRateHz, 16);

            double max = GetMaxValue();
			double mult = short.MaxValue /max;

            //double avg = Samples.Average(d => Math.Abs(d));
            //double mult = short.MaxValue * 0.3 / avg;

            for (int i = 0; i < Samples.Length; i++ )
            {
               // dest.SeekToAudioSample(i);
                dest.AddSample_16bit((short) (Samples[i]*mult));
            }

            dest.Close();
        }

		public double GetMaxValue()
        {
			double max = 0;
            for(int i = 0; i<Samples.Length; i++)
            {
                max = Math.Max(max, Math.Abs(Samples[i]));

                
            }

            return max;
        }

		public double GetSum()
        {
			double max = 0;
            for (int i = 0; i < Samples.Length; i++)
            {
                max += Samples[i];


            }

            return max;

        }
		public double GetAvg()
        {
			double max = 0;
            for (int i = 0; i < Samples.Length; i++)
            {
                max += Math.Abs( Samples[i] );


            }

            return max/Samples.Length;

        }
        public void Center()
        {
			double ofs = GetSum() / Samples.Length;

            for (int i = 0; i < Samples.Length; i++)
            {
                Samples[i] -= ofs;
            }


            
        }
        public void Mult(double mult)
        {

            for(int i = 0; i<Samples.Length; i++)
            {
                Samples[i] = (Samples[i] * mult);
            }
    
        }
        public void NormToShort()
        {
        	double avg = Samples.Average(d => Math.Abs(d));

       
            double mult = short.MaxValue * 0.3 / avg;
            Mult(mult);

        }

    	public WFile InitNew()
    	{
			var result = new WFile
			{
				SampleRateHz = this.SampleRateHz,
				Samples = new double[this.Samples.Length]
			};
    		return result;
    	}
    	public WFile Clone()
    	{
			var result = new WFile
			{
				SampleRateHz = this.SampleRateHz,
				Samples = new double[this.Samples.Length]
			};
			for (int i = 0; i < Samples.Length; i++)
			{
				result.Samples[i] = Samples[i];
			}

    		return result;
    	}
        public WFile SubString(int start, int len)
        {
            var result = new WFile
            {
                SampleRateHz = this.SampleRateHz,
                Samples = new double[len]
            };
            for (int i = 0; i < len; i++)
            {
                int p = start + i;
                if (p >= this.Samples.Length)
                    break;

                result.Samples[i] = Samples[p];
            }

            return result;
        }



        public void Diff()
        {
         
			double prev = 0;
            for(int i = 0; i<Samples.Length; i++)
            {
            	var delta = this.Samples[i] - prev;
            	prev = this.Samples[i];
                this.Samples[i] = delta;
            }

          

        }       
        public void DiffM()
        {

            double prev = 0;
            for(int i = 0; i<Samples.Length-1; i++)
            {
            	var delta = Samples[i+1] - prev;
            	prev = this.Samples[i];
                this.Samples[i] = delta/2;
            }
  
            Samples[Samples.Length - 1] = 0;

          

        }
        public void DiffR()
        {
         
			double prev = 0;
            for (int i = Samples.Length; i-- > 0;  )
            {
                var delta = this.Samples[i] - prev;
                prev = this.Samples[i];
                this.Samples[i] = -delta;
            }

          

        }

        public void Diff2()
        {
            Diff();
            DiffR();
        }

        public void DiffNorm()
        {
            var a = GetAvg();
			double prev = 0;
            for(int i = 0; i<Samples.Length; i++)
            {
            	var delta = this.Samples[i] - prev;
            	prev = this.Samples[i];
                this.Samples[i] = delta;
            }

            var b = GetAvg();
            Mult(a/b);

        }

        public void Integrate()
        {
 
			double sum = 0;
            for (int i = 0; i < Samples.Length; i++)
            {
                sum += this.Samples[i];
                this.Samples[i] = sum;
                

            }
            

        }
		public class SD : SortedDictionary<double, int>
		{
			 public void Inc(double key)
			 {
				 if (ContainsKey(key))
				 {
				 	this[key] ++;
					 return;

				 }
				 
				 Add(key, 1);
			 

			 }
			 public void Dec(double key)
			 {
			 	int r = (this[key] -= 1);
				if (r == 0)
					this.Remove(key);
				 

			 }



		}
		public double GetMaxN(int n)
		{
			int cnt = 0;
			var result = new SD();
			foreach (double t in Samples)
			{
				var v = Math.Abs(t);

				if(cnt < n)
				{
					result.Inc(v);

					cnt++;

					continue;
				}

				var min = result.Keys.First();
				if(v > min)
				{
					result.Dec(min);
					result.Inc(v);
					

				}
			}

			return result.Keys.First();
		}

		public void RemoveTopN(int n)
		{
			var max = GetMaxN(n);
			for(int i = 0; i<Samples.Length; i++)
			{
				var v = Math.Abs(Samples[i]);
				if(v < max)
					continue;

				var prev = i == 0 ? 0 : Samples[i - 1];
				Samples[i] = prev;


			}



		}
		public void DiffInt(int cnt)
		{
			for (int i = 0; i < cnt; i++)
			{
				DiffM();
                RemoveTopN(100);
			}

			for (int i = 0; i < cnt; i++)
			{
				Integrate();
			}


		}
        public struct D
        {
            public int Pos;
			public int Len;
			public double Area;
        }
        public IEnumerable<D> Invert()
        {
            bool prevSign = true;
            int len = 0;
			double area = 0;
            for (int i = 0; i < Samples.Length; i++)
            {
                bool sign = Samples[i] < 0;
                if(sign != prevSign)
                {
                    yield return new D {Area = Math.Abs( area), Len = len, Pos = i};
                    len = 0;
                    area = 0;
                }

                prevSign = sign;
                len++;
                area += Samples[i];

                
            }

        }

        public Bitmap DrawInvert(int w, int h)
        {
            int maxX = Samples.Length;
           // int maxY = Math.Log10(10000);
            var result = new Bitmap(w, h);
            using(var g = Graphics.FromImage(result))
                g.FillRectangle(Brushes.Black, 0, 0, w, h);
            foreach (D sample in Invert())
            {
                if(sample.Len == 0 || sample.Area == 0)
                    continue;

                long x = ((long) sample.Pos*w)/maxX;
                double hz = (double)this.SampleRateHz/(double)sample.Len;
                double lnGz = Math.Log10(hz);
                //double y = (double)h * lnGz / (double)4;
                double y = h - 1 - hz/10 ;
                if (y < 0)
                    y = 0;

                double color = (double) sample.Area/sample.Len/15;
                int cl =  (int)color;
                if (cl > 255)
                    cl = 255;


                result.SetPixel((int)x, (int)y, Color.FromArgb(cl, cl, cl));

         
            }

            return result;
        }

        public void ToPolar(out WFile radius, out WFile phase)
        {
            radius = this.InitNew();
            phase = this.InitNew();

            var d = this.Clone();
            d.Diff();

            for(int i = 0; i<Samples.Length; i++)
            {
                var x = this.Samples[i];
                var y = d.Samples[i];
                radius.Samples[i] = Math.Sqrt(x*x + y*y);
                phase.Samples[i] = Math.Atan2(y, x);
            }


        }

		public bool FindIMF()
		{

			var topX = new List<double>();
			var topY = new List<double>();
			var botX = new List<double>();
			var botY = new List<double>();

			
	
	

			for (int i = 1; i < Samples.Length -1; i++)
			{
				var p = Samples[i - 1];
				var s = Samples[i];
				var n = Samples[i + 1];
				bool isTop = p < s && s > n;
				bool isBot = p > s && s < n;

				
				if (isTop || isBot)
				{
					if(isTop)
					{
						topX.Add(i);
						topY.Add(s);

					}
					if(isBot)
					{
						botX.Add(i);
						botY.Add(s);
						
					}

	

				}
			}

			if (topX.Count < 3 || botX.Count < 3)
				return false;

			var topSpline = new CubicSpline();
			topSpline.BuildSpline(topX.ToArray(), topY.ToArray(), topX.Count);
			var botSpline = new CubicSpline();
			botSpline.BuildSpline(botX.ToArray(), botY.ToArray(), botX.Count);

			for(int i = 0; i<Samples.Length; i++)
			{
				double m = topSpline.Func(i) + botSpline.Func(i);
				Samples[i] -= m/2;
			}

			return true;


		}

		public WFile GetIMF()
		{
			var result = Clone();
			for (int i = 0; i < 6; i++)
			{
				bool b = result.FindIMF();
				if(!b)
					break;
				
			}

			for(int i = 0; i<Samples.Length; i++)
			{
				Samples[i] -= result.Samples[i];
			}

			return result;
		}
        public void ToCenter(double periodSec)
        {
            var xlist = new List<double>();
            var ylist = new List<double>();

            xlist.Add(0);
            ylist.Add(0);

            int range = (int) (periodSec*this.SampleRateHz);
            for(int n = 0; (n+1)*range < Samples.Length; n++)
            {
                double min = Samples[n*range];
                double max = Samples[n*range];
            

                for(int i = 0; i<range; i++)
                {
                    double s = Samples[n*range + i];
                    min = Math.Min(min, s);
                    max = Math.Max(max, s);


                }

                double avg = (min + max)/2;
                xlist.Add(n*range + range/2);
                ylist.Add(avg);

            }


            xlist.Add(Samples.Length -1);
            ylist.Add(0);

            var spline = new CubicSpline();
            spline.BuildSpline(xlist.ToArray(), ylist.ToArray(), xlist.Count);

            for(int i = 0; i<Samples.Length; i++)
            {
                Samples[i] -= spline.Func(i);
            }

            for (int i = 0; i < range; i++)
                Samples[Samples.Length - 1 - i] = 0;
        }
        public void BeforeIntegrate(double periodSec)
        {
            int range = (int)(periodSec * SampleRateHz);
            int tailRange = range/10;

            int start = 0;
            while (start + range + tailRange < this.Samples.Length)
            {
                double sum = 0;
                for(int i = 0; i<range; i++)
                {
                    sum += this.Samples[start + i];
                }

                double avg = sum/range;
                int ofs = 0;

                for(int i = 0; i<tailRange; i++)
                {
                    //if (start + range + i >= Samples.Length)
                    //    break;


                    sum += this.Samples[start + range + i];
                    double avg2 = sum/(range + i);
                    if(Math.Abs(avg2) < Math.Abs(avg))
                    {
                        ofs = i;
                        avg = avg2;
                    }
                }

                

               // double ofs;
                for (int i = 0; i < range+ofs; i++)
                {
                   // ofs = (Math.Cos(Math.PI*i/samples) -1) * avg / 2/samples;
                    this.Samples[start + i] -= avg;
                }

                //double sum2 = 0;
                //for (int i = 0; i < samples; i++)
                //{
                //    sum2 += this.Samples[start + i];
                //}

                //if(sum2!=0 && Math.Abs(sum2/sum) > 0.01)
                //    throw new Exception();

                start +=  range + ofs;
            }

            for (int i = start; i < Samples.Length; i++)
                Samples[i] = 0;

        }


	    void ToSinus(WFile interpolation, WFile outDelta)
        {

            double frameRate = 300;
            int frameLen = (int) (this.SampleRateHz/frameRate);
            int framesCount = this.Samples.Length/frameLen;

            double maxAmp = this.Samples.Max();

           // Parallel.For(0, framesCount, frame =>
                                          for(int frame = 0; frame < framesCount; frame++)
                                             {
                                                 var offsetInFrame = frame*frameLen;

                                                 bool hasData = false;
                                                 for (int i = 0; i < frameLen; i++)
                                                 {
                                                     var s = this.Samples[offsetInFrame + i];
                                                     if (s != 0)
                                                     {
                                                         hasData = true;
                                                         break;
                                                     }





                                                 }

                                                 if (!hasData)
                                                     return;

                                                 var model = new Model
	                                                 {
		                                                 MaxAmp = maxAmp,
		                                                 MinPeriod = 4,
		                                                 MaxPeriod = this.SampleRateHz/100,
		                                                 frameLen = frameLen,
		                                                 offsetInFrame = offsetInFrame,
		                                                 samples = this.Samples
	                                                 };

	                                             for (int i = 0; i < 3; i++)
                                                 {
                                                     var w = new Wave
                                                                 {
                                                                     Model = model,
                                                                    
                                                                     Prev = model.Waves.LastOrDefault()
                                                                 };
                                                     model.Waves.Add(w);
                                                 }

                                                 var eq = new EqSolver
	                                                 {
		                                                 Model = model,
		                                                 MaxIter = 90000
	                                                 };


	                                             eq.MonteCarlo();

												 //eq.Iter = 0;
												 //eq.MaxIter = 6000;
												 //eq.Descent();

												 eq.Iter = 0;
                                                 eq.MaxIter = 60000;
                                                 eq.DescentRandomDir();

                                                 for (int i = 0; i < frameLen; i++)
                                                 {
                                                     var s = this.Samples[offsetInFrame + i];
                                                     var v = model.Waves.Sum(w => w.GetValue(i));

                                                     double delta = s - v;

                                                     interpolation.Samples[offsetInFrame + i] = v;
                                                     outDelta.Samples[offsetInFrame + i] = delta;





                                                 }
                                             }
			//);



        }

	    public static void AutoCorr(WFile src, int frameSize, int offset, string folder)
		{
			if (!Directory.Exists(folder))
				Directory.CreateDirectory(folder);


			int numberOfPoints = (src.Samples.Length - frameSize)/frameSize;
			int numberOfLines = (src.Samples.Length - frameSize)/offset;

			int bitmapHeight = 600;
			int bitmapWidth = 800;

			int bitmapCountX = numberOfPoints/bitmapWidth;
			int bitmapCountY = numberOfLines/bitmapHeight;

			var b = new Bitmap(bitmapWidth, bitmapHeight, PixelFormat.Format16bppRgb555);
			var line = new double[bitmapWidth];

			for (int by = 0; by < bitmapCountY; by++ )
			{for (int bx = 0; bx < bitmapCountX; bx++)
			{
				
				for(int y =0; y< bitmapHeight; y++)
				{
						
						for (int x = 0; x < bitmapWidth; x++)
						{
							int aOfs = (bx*bitmapWidth + x)*offset;
							int bOfs = (by*bitmapHeight + y)*offset;

							double e1 = 0;
							double e2 = 0;
							double p = 0;
							for (int f = 0; f < frameSize; f++)
							{

								double sa = src.Samples[aOfs + f];
								double sb = src.Samples[bOfs + f];
								p += sa*sb;
								e1 += sa*sa;
								e2 += sb*sb;

							}

							double norm = Math.Sqrt(e1*e2);
							double v = p/norm;
							Color cl = GetColor(Math.Abs(v));
							b.SetPixel(x, y, cl);
							


						}

					}

				string fn = String.Format("img{0}_{1}.bmp", bx, by);
				string path = Path.Combine(folder, fn);
				b.Save(path);





			}}
		}

	    static Color GetColor(double d)
	    {
		    if (d >= 1)
			    return Color.White;
		    if (d == 0)
			    return Color.Black;

		    if (d < 0.1)
			    return Color.BlueViolet;

		    if (d < 0.3)
			    return Color.CadetBlue;

		    if (d < 0.5)
			    return Color.Chartreuse;

		    if (d < 0.7)
			    return Color.Crimson;

		 
		    return Color.Cyan;

			//if (d == 0)
			//	return Color.Black;

			//if (d == 0)
			//	return Color.Black;


			//int v =(int) (d*(1<<20));
			//return Color.FromArgb(v & 0xf, (v >> 8) & 0xf, (v >> 16) & 0xf);
	    }
    }


}
